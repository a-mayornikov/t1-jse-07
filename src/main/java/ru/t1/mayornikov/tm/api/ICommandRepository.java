package ru.t1.mayornikov.tm.api;

import ru.t1.mayornikov.tm.model.Command;

public interface ICommandRepository {

    Command[] getCommands();

}